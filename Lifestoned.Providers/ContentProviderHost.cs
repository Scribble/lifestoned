/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Lifestoned.DataModel.Shared;

namespace Lifestoned.Providers
{
    public class ContentProviderHost
    {
        private static Dictionary<string, IGenericContentProvider> providers = new Dictionary<string, IGenericContentProvider>();

        public static void AddProvider(string key, IGenericContentProvider provider)
        {
            providers.Add(key, provider);
        }

        public static IGenericContentProvider GetProvider(string key)
        {
            providers.TryGetValue(key, out IGenericContentProvider result);
            return result;
        }

        public static T GetProvider<T>(string key) where T : class, IGenericContentProvider
        {
            providers.TryGetValue(key, out IGenericContentProvider result);
            return result as T;
        }

        public static IQueryable GetContent(string type)
        {
            return GetProvider(type)?.GetContent();
        }

        public static object GetContent(string type, uint id)
        {
            return GetProvider(type)?.GetContent(id);
        }

        public static bool SaveContent(string type, object item)
        {
            return GetProvider(type)?.SaveContent(item) == true;
        }

        public static bool UpdateContent(string type, object item)
        {
            return GetProvider(type)?.UpdateContent(item) == true;
        }

        public static bool DeleteContent(string type, uint id)
        {
            return GetProvider(type)?.DeleteContent(id) == true;
        }

        public static IContentProvider CurrentProvider { get; set; }

        public static IManagedWorldProvider ManagedWorldProvider { get; set; }

		public static IRecipeProvider CurrentRecipeProvider { get; set; }
		public static ISpawnMapProvider CurrentSpawnMapProvider { get; set; }
		public static IWeenieProvider CurrentWeenieProvider { get; set; }
		public static ISpellTableProvider CurrentSpellTableProvider { get; set; }
		public static IPatchContentProvider PatchContentProvider { get; set; }
    }
}
