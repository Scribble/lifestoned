/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Gdle.Spawns;
using Lifestoned.DataModel.Shared;
using Lifestoned.DataModel.WorldRelease;

namespace Lifestoned.Providers
{
	public interface IGenericContentProvider
    {
        IQueryable GetContent();
        object GetContent(uint id);
        bool SaveContent(object item);
        bool UpdateContent(object item);
        bool DeleteContent(uint id);
    }

	public interface IGenericContentProvider<T> : IGenericContentProvider where T : class
    {
        IQueryable<T> Get();
        T Get(uint id);
        bool Save(T item);
        bool Update(T item);
        bool Delete(uint id);

        IQueryable<T> Search(Expression<Func<T, bool>> criteria);
        IQueryable<T> Search(Expression<Func<T, bool>> criteria, int limit);
    }

    public interface IContentProvider //: ISpawnMapProvider
    {
		int BusyCount { get; }

        List<WeenieSearchResult> WeenieSearch(string token, SearchWeeniesCriteria criteria);

        List<WeenieSearchResult> RecentChanges(string token);

        List<WeenieSearchResult> AllUpdates(string token);

        Weenie GetWeenie(string token, uint weenieClassId);

        bool UpdateWeenie(string token, Weenie weenie);

        bool CreateWeenie(string token, Weenie weenie);

        bool DeleteWeenie(string token, uint weenieClassId);
        
        byte[] GetWorldRelease(string token, string fileName);

        byte[] GetCurrentWorldRelease(string token);

        Release GetCurrentWorldReleaseInfo(string token);

        Release GetWorldReleaseInfo(string token, string fileName);

        Release CutWorldRelease(string token, ReleaseType releaseType);

        SpawnMapEntry GetLandblock(uint id);
        void SaveLandblock(SpawnMapEntry spawnMap);
        void DeleteLandblock(uint id);

        List<SpawnMapEntry> SearchLandblocks(string query);
        IEnumerable<SpawnMapEntry> GetLandblocks();
    }
}
