﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;

using Lifestoned.DataModel.Gdle.Spawns;

namespace Lifestoned.Providers.Database
{
    public class DatabaseSpawnMapProvider : SQLiteContentDatabase<SpawnMapEntry>, ISpawnMapProvider
    {
        public DatabaseSpawnMapProvider() : base("SpawnMapDbConnection", "SpawnMaps", o => o.Key)
        {
        }

        #region ISpawnMapProvider

        public void DeleteLandblock(uint id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                    db.Execute("DELETE FROM SpawnMaps WHERE landblockId=@id", new { id });
            }
            catch (DbException)
            {
            }
        }

        public SpawnMapEntry GetLandblock(uint id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SpawnMapEntry> GetLandblocks()
        {
            throw new NotImplementedException();
        }

        public void SaveLandblock(SpawnMapEntry spawnMap)
        {
            throw new NotImplementedException();
        }

        public List<SpawnMapEntry> SearchLandblocks(string query)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class DatabaseSpawnMapSandboxProvider : SQLiteSandboxDatabase<SpawnMapEntry, SpawnMapChange>, ISpawnMapSandboxProvider
    {

        public DatabaseSpawnMapSandboxProvider() : base("SpawnMapDbConnection", "SpawnMaps_Sandbox", (o) => o.Key)
        {
        }

        #region ISpawnMapProvider

        public void DeleteLandblock(uint id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                    db.Execute("DELETE FROM SpawnMaps WHERE landblockId=@id", new { id });
            }
            catch (DbException)
            {
            }
        }

        public SpawnMapEntry GetLandblock(uint id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SpawnMapEntry> GetLandblocks()
        {
            throw new NotImplementedException();
        }

        public void SaveLandblock(SpawnMapEntry spawnMap)
        {
            throw new NotImplementedException();
        }

        public List<SpawnMapEntry> SearchLandblocks(string query)
        {
            throw new NotImplementedException();
        }

        public SpawnMapChange GetLandblockChange(uint id)
        {
            throw new NotImplementedException();
        }

        public SpawnMapChange GetLandblockChange(uint id, string userId)
        {
            throw new NotImplementedException();
        }

        #endregion

    }

}
