/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc.Filters;
using System.Web.Security;
using Lifestoned.DataModel.Account;
using Lifestoned.DataModel.Shared;
using log4net;

namespace Lifestoned.Providers
{
    public class AuthProviderHost
    {
        private const string _Session_Account = "__account";

        public static int TokenLifespan = 3600;

        public static IAuthProvider PrimaryAuthProvider { get; set; }

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// holy hackery, batman!  this is the cross-library glue that holds authentication together.  See BaseController.cs
        /// this could also use HttpContext.Current.Items instead of session
        /// </summary>
        public static ApiAccountModel CurrentUser
        {
            get { return (ApiAccountModel)HttpContext.Current.Session[_Session_Account]; }
            set { HttpContext.Current.Session[_Session_Account] = value; }
        }

        /// <summary>
        /// reads the configuration for a provider type and sets the primary auth
        /// provider accordingly
        /// </summary>
        static AuthProviderHost()
        {
            try
            {
                int.TryParse(ConfigurationManager.AppSettings["TokenLife"], out TokenLifespan);
            }
            catch
            {
            }

            string providerType = ConfigurationManager.AppSettings["AuthProvider"];

            try
            {
                object authProvider = Activator.CreateInstance(Type.GetType(providerType));
                PrimaryAuthProvider = (IAuthProvider)authProvider;
            }
            catch (Exception ex)
            {
                log.Error($"Unable to load Auth Provider Host {providerType}", ex);
                throw;
            }
        }

		public static bool Authenticate(string username, string password)
        {
            ApiAccountModel account = PrimaryAuthProvider.Authenticate(username, password);

            if (account == null)
                return false;

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
				1, username, DateTime.Now, DateTime.Now.AddSeconds(TokenLifespan), false, account.AccountGuid);

            HttpCookie cookie = new HttpCookie(
                FormsAuthentication.FormsCookieName,
                FormsAuthentication.Encrypt(ticket));

            HttpContext.Current.Response.AppendCookie(cookie);

            FormsIdentity identity = new FormsIdentity(ticket);
            List<SubscriptionModel> subs = PrimaryAuthProvider.GetSubscriptions(null, account.AccountGuid);

            UpdateIdentity(identity, account, subs);

            HttpContext.Current.User = new ClaimsPrincipal(identity);

            return true;
        }

        public static void SignOut()
        {
            FormsAuthentication.SignOut();
        }

		public static string GetUserUid()
        {
            return GetUserUid(HttpContext.Current.User.Identity as ClaimsIdentity);
        }

		public static string GetUserDisplayName()
        {
            if (HttpContext.Current.User.Identity is ClaimsIdentity identity)
                return identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            return null;
        }

		public static string GetUserUid(ClaimsIdentity identity)
        {
            if (identity == null)
                return null;

            if (identity is FormsIdentity fi)
                return fi.Ticket.UserData;

            return identity.Claims.FirstOrDefault(c => c.Type == "")?.Value;
        }

        private static void UpdateIdentity(ClaimsIdentity identity, ApiAccountModel account, List<SubscriptionModel> subscriptions)
        {
			foreach (var sub in subscriptions)
            {
				foreach (AccessLevel level in Enum.GetValues(typeof(AccessLevel)))
                {
                    if (sub.AccessLevel >= (int)level)
                        identity.AddClaim(new Claim(ClaimTypes.Role, level.ToString()));
                }
            }
			// FormsIdentity uses ClaimTypes.Name for usernames
			// Ticket UserData contains the account uid
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, account.AccountGuid));
            identity.AddClaim(new Claim(ClaimTypes.Email, account.Email));

            //	  new Claim(ClaimTypes.Name, displayName),
            //    new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
            //        "DerethForever.Auth"),
            //    new Claim("token", token)
        }

        public class AuthenticationFilter : IAuthenticationFilter
        {
            public void OnAuthentication(AuthenticationContext filterContext)
            {
                HttpCookie cookie = filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (cookie == null)
                    return;

                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

                if (ticket.Expired)
                {
                    SignOut();
                    return;
                }

                FormsIdentity identity = new FormsIdentity(ticket);

                CurrentUser = PrimaryAuthProvider.GetAccount(null, ticket.UserData);
                List<SubscriptionModel> subs = PrimaryAuthProvider.GetSubscriptions(null, ticket.UserData);

                UpdateIdentity(identity, CurrentUser, subs);

                filterContext.Principal = new ClaimsPrincipal(identity);
            }

            public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
            {
                // no-op
            }
        }
    }
}
