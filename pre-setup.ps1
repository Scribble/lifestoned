[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")|Out-Null

Function Get-Folder() {

    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
    $foldername.Description = "Select a base folder for your Lifestoned data to be installed"
    $foldername.rootfolder = "MyComputer"

    if($foldername.ShowDialog() -eq "OK")
    {
        $folder += $foldername.SelectedPath
    }
    return $folder
}

function Get-File() {
    $FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{
        Multiselect = $false
        Filter = 'AC Client Portal Data|client_portal.dat'
    }
     
    [void]$FileBrowser.ShowDialog()
    
    $file = $FileBrowser.FileName;
    
    If( -Not ($FileBrowser.FileNames -like "*\*")) {
        Write-Error "Client Cancelled"
        exit 1
    }
    return $file
}


Write-Host "Creating Lifestoned Data Directories..."
$base_directory = Get-Folder
$data_directory = Join-Path -Path $base_directory -ChildPath "data"
New-Item -ErrorAction Ignore -Path $base_directory -Name "data" -ItemType "directory"
if (-Not (Test-Path $data_directory)) {
    Write-Error "Issue creating directory at provided location ($data_directory)"
    exit 1
}

$sub_directories = @("Sandboxes", "WorldReleases", "LocalAuthProvider", "Final")
foreach ($dir in $sub_directories) {
    New-Item -ErrorAction Ignore -Path $data_directory -Name $dir -ItemType "directory"
    if (-Not (Test-Path (Join-Path $data_directory $dir))) {
        Write-Error "Issue creating directory at provided location ($d)"
        exit 1
    }
}

Write-Host "Copying required client data..."

$client_dat = Get-File
if (Test-Path $client_dat) {
    Copy-Item $client_dat -Destination $data_directory
} else {
    Write-Error "Couldn't find client_portal.dat at the provided location: ($client_dat)"
    exit 1
}

if (-Not (Test-Path (Join-Path $data_directory "client_portal.dat"))) {
    Write-Error "Issue copying client data to the following location: ($client_dat)"
    exit 1
}

Write-Host "Pre-Setup Complete! glhf!"