﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.filter('toDate', function (value) {
    var d = value;
    if (!d) {
        return 'n/a';
    }
    if (typeof value === 'string') {
        if (value.startsWith('/Date')) {
            // ms-json
            d = parseInt(value.substring(6, value.length - 2));
        } else {
            // iso
            d = new Date(value);
        }
    }
    if (typeof d === 'number') {
        d = new Date(d);
    }
    return `${d.getFullYear()}-${(d.getMonth()+1).pad(2)}-${d.getDate().pad(2)}`;
});

Vue.filter('f2', function (value) {
    var d = value;
    if (!d && d !== 0) {
        return 'NaN';
    }
    if (typeof value === 'string') {
        // ms-json
        d = parseFloat(value);
    }
    return d.toFixed(2);
});

Vue.filter('yn', function (value) {
    var d = value;
    return d ? 'Yes' : 'No';
});