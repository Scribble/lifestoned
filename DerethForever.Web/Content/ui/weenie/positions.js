﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-positions', {
    props: ['vals'],
    model: { prop: 'vals', event: 'changed' },
    methods: {
        addNew(key) {
            return {
                key: key,
                value: this.$weenie.newPosition()
            };
        }
    },
    template: `
    <lsd-props-base v-model="vals" name="Position" type="PositionType" :newfn="addNew">
        <lsd-position slot-scope="p" v-model="p.item.value" />
    </lsd-props-base>
    `
});
