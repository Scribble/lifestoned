﻿
/// <reference path="../../../Scripts/vue.js" />

const VITAL_CALC = [
    (a) => a.endurance.init_level / 2,
    (a) => a.endurance.init_level,
    (a) => a.self.init_level
];

const SKILL_CALC = {
    6: (a) => (a.coordination.init_level + a.quickness.init_level) / 3,
    7: (a) => (a.coordination.init_level + a.quickness.init_level) / 5,
    14: (a) => (a.focus.init_level) / 3,
    15: (a) => (a.focus.init_level + a.self.init_level) / 7,
    16: (a) => (a.focus.init_level + a.self.init_level) / 6,
    21: (a) => (a.coordination.init_level + a.focus.init_level) / 3,
    22: (a) => (a.coordination.init_level + a.strength.init_level) / 2,
    23: (a) => (a.coordination.init_level + a.focus.init_level) / 3,
    24: (a) => (a.quickness.init_level),
    31: (a) => (a.focus.init_level + a.self.init_level) / 4,
    32: (a) => (a.focus.init_level + a.self.init_level) / 4,
    33: (a) => (a.focus.init_level + a.self.init_level) / 4,
    34: (a) => (a.focus.init_level + a.self.init_level) / 4,
    37: (a) => (a.focus.init_level + a.coordination.init_level) / 3,
    38: (a) => (a.focus.init_level + a.coordination.init_level) / 3,
    39: (a) => (a.focus.init_level + a.coordination.init_level) / 3,
    41: (a) => (a.strength.init_level + a.coordination.init_level) / 3,
    43: (a) => (a.focus.init_level + a.self.init_level) / 4,
    44: (a) => (a.strength.init_level + a.coordination.init_level) / 3,
    45: (a) => (a.strength.init_level + a.coordination.init_level) / 3,
    46: (a) => (a.coordination.init_level + a.quickness.init_level) / 3,
    47: (a) => (a.coordination.init_level) / 2,
    48: (a) => (a.strength.init_level + a.coordination.init_level) / 2,
    49: (a) => (a.coordination.init_level + a.coordination.init_level) / 3,
    50: (a) => (a.strength.init_level + a.quickness.init_level) / 3,
    51: (a) => (a.coordination.init_level + a.quickness.init_level) / 3,
    52: (a) => (a.strength.init_level + a.coordination.init_level) / 3,
    54: (a) => (a.endurance.init_level + a.self.init_level) / 3,
};

Vue.component('lsd-attr', {
    props: ['attribute', 'name'],
    model: { prop: 'attribute', event: 'changed' },
    template: `
    <div>
        <div class="col-md-2">{{ name }}</div>
        <div class="col-md-2">
            <input v-model.number="attribute.init_level" type="text" class="form-control" />
        </div>
        <slot :attribute="attribute"></slot>
    </div>
    `
});

Vue.component('lsd-vital', {
    props: {
        'vital': { type: Object },
        'name': { type: String },
        'calcBase': { type: Function, default: () => 0 }
    },
    model: { prop: 'vital', event: 'changed' },
    template: `
    <lsd-attr v-model="vital" :name="name">
    <template slot-scope="p">
        <div class="col-md-2">
            <input v-model.number="p.attribute.current" type="text" class="form-control" />
        </div>
        <div class="col-md-2">
            <input :value="p.attribute.init_level + calcBase()" type="text" class="form-control" readonly />
        </div>
    </template>
    </lsd-attr>
    `
});

Vue.component('lsd-attributes', {
    props: ['attributes'],
    model: { prop: 'attributes', event: 'changed' },
    methods: {
        calc(idx) {
            return Math.floor(VITAL_CALC[idx](this.attributes));
        }
    },
    template: `
    <div>
        <div class="row row-spacer">
            <div class="col-md-2"><strong>Attribute</strong></div>
            <div class="col-md-2"><strong>Base</strong></div>
            <div class="col-md-2"><strong>Vital</strong></div>
            <div class="col-md-2"><strong>Base</strong></div>
            <div class="col-md-2"><strong>Current</strong></div>
            <div class="col-md-2"><strong>Max</strong></div>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Strength" :attribute="attributes.strength"></lsd-attr>
        <lsd-vital name="Health" v-model="attributes.health" :calc-base="() => calc(0)"></lsd-vital>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Endurance" :attribute="attributes.endurance"></lsd-attr>
        <lsd-vital name="Stamina" v-model="attributes.stamina" :calc-base="() => calc(1)"></lsd-vital>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Coordination" :attribute="attributes.coordination"></lsd-attr>
        <lsd-vital name="Mana" v-model="attributes.mana" :calc-base="() => calc(2)"></lsd-vital>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Quickness" :attribute="attributes.quickness"></lsd-attr>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Focus" :attribute="attributes.focus"></lsd-attr>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Self" :attribute="attributes.self"></lsd-attr>
        </div>
    </div>
    `
});

Vue.component('lsd-skill', {
    props: {
        'skill': { type: Object },
        'calcBase': { type: Function, default: () => 0 }
    },
    model: { prop: 'skill', event: 'changed' },
    template: `
    <div class="row row-spacer">
        <div v-lsd-enum-display="{ type: 'SkillId', key: skill.key }" class="col-md-2">Unknown/Removed ({{skill.key}})</div>
        <div class="col-md-2">
            <lsd-enum-select type="SkillStatus" v-model="skill.value.sac" keyOnly></lsd-enum-select>
        </div>
        <div class="col-md-2">
            <input v-model.number="skill.value.init_level" type="text" class="form-control" />
        </div>
        <div class="col-md-2">
            <input :value="skill.value.init_level + calcBase()" type="text" class="form-control" readonly />
        </div>
        <lsd-prop-delete @click="$emit('removed')"></lsd-prop-delete>
    </div>
    `
});

Vue.component('lsd-skills', {
    props: ['skills', 'attributes'],
    model: { prop: 'skills', event: 'changed' },
    methods: {
        addNew(event) {
            var skills = this.skills || [];
            skills.push(this.$weenie.newSkill(event));
            if (!this.skills) this.$emit('changed', skills);
        },
        calc(id) {
            var fn = SKILL_CALC[id] || ((a) => 0);
            return Math.floor(fn(this.attributes));
        },
        removed(idx) {
            this.skills.splice(idx, 1);
        }
    },
    template: `
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Skills</h3></div>
        <div class="panel-body">
            <div class="row row-spacer">
                <div class="col-md-2"><strong>Name</strong></div>
                <div class="col-md-2"><strong>Spec</strong></div>
                <div class="col-md-2"><strong>Base</strong></div>
                <div class="col-md-2"><strong>Effective</strong></div>
            </div>
            <lsd-skill v-for="(skill, idx) in skills" :key="skill.key" :idx="idx" v-model="skills[idx]" :calc-base="() => calc(skill.key)" @removed="removed(idx)"></lsd-skill>
        </div>
        <lsd-prop-add :vals="skills" name="Skill" type="SkillId" @added="addNew"></lsd-prop-add>
    </div>
    `
});
