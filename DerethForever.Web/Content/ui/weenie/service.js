﻿
/// <reference path="../../../Scripts/vue.js" />

function makeWeenieService(id, clone, userId) {
    return new Vue({
        data() {
            return {
                weenieId: id,
                weenie: {
                    wcid: null,
                    weenieClassId: null,
                    weenieTypeId: null,
                    name: null,
                    iconId: null,
                    modifiedBy: null,
                    lastModified: null,
                    isDone: false,
                    stringStats: [],
                    intStats: [],
                    int64Stats: [],
                    floatStats: [],
                    iidStats: [],
                    didStats: [],
                    boolStats: [],
                    posStats: []
                },
                isNew: true,
                isClone: clone,
                userId
            };
        },
        watch: {
            'weenie.weenieTypeId': function (val, old) {
                //this.weenie.WeenieType_Binder = val;
                this.weenie.weenieType = val;
            },
            'weenie.name': function (val, old) {
                // verify the string prop exists
                var name = this.weenie.stringStats.find(function (stat) {
                    if (stat.key === 1)
                        return stat;
                });

                if (!name) {
                    name = {
                        key: 1,
                        value: ''
                    };
                    this.weenie.stringStats.push(name);
                }
                name.value = val;
            },
            'weenie.intStats': {
                deep: true,
                handler(val, old) {
                    var item = this.weenie.intStats.find((v) => v.key === 1);
                    if (item) this.weenie.itemType = item.value;
                }
            }
        },
        methods: {
            fetch(id) {
                var $this = this;
                $this.weenieId = id;
                $.getJSON("/Weenie/Get", { id: $this.weenieId, userGuid: $this.userId },
                    function (res) {
                        if (!res.posStats) res.posStats = [];
                        $this.weenie = Object.assign({}, $this.weenie, res);
                        $this.weenie.name = res.stringStats.find((v) => v.key === 1).value;

                        if ($this.isClone) {
                            $this.weenieId = null;
                            $this.weenie.weenieClassId = null;
                            $this.weenie.lastModified = null;
                            $this.weenie.changelog = [];
                        } else {
                            $this.isNew = false;
                        }
                    });
            },
            save() {
                $.ajax({
                    type: this.isNew ? "POST" : "PUT",
                    url: this.isNew ? "/Weenie/New" : "/Weenie/Put",
                    //data: this.weenie,
                    data: JSON.stringify(this.weenie),
                    contentType: 'application/json',
                    //dataType: 'json',
                    success: function (data, status, xhr) {
                        console.log(data, status);
                        alert('Saved Successfully');
                        //window.location.href = '/Sandbox';
                    },
                    error: function (xhr, status, err) {
                        console.error(status, err);
                        alert('Save Failed');
                    }
                });
            },

            validate() {
                // item type is required
                if (this.weenie.intStats.find((o) => o.key === 1) === null)
                    return false;

                return true;
            },

            newFrame() {
                return {
                    origin: { x: 0.0, y: 0.0, z: 0.0 },
                    angles: { w: 1.0, x: 0.0, y: 0.0, z: 0.0 }
                    //Display: null//'[0.000000 0.000000 2.000000] 1.000000 0.000000 0.000000 0.000000'
                };
            },
            newPosition() {
                return {
                    objcell_id: 0,
                    frame: this.newFrame()
                    //Display: null//'0x00000000 [0.000000 0.000000 2.000000] 1.000000 0.000000 0.000000 0.000000'
                };
            },

            newBodyPart() {
                return {
                    dtype: null,
                    dval: null,
                    dvar: null,
                    bh: null,
                    acache: {
                        base_armor: null,
                        armor_vs_bludgeon: null,
                        armor_vs_pierce: null,
                        armor_vs_slash: null,
                        armor_vs_acid: null,
                        armor_vs_cold: null,
                        armor_vs_fire: null,
                        armor_vs_electric: null,
                        armor_vs_nether: null
                    },
                    bpsd: {
                        HLF: null,
                        MLF: null,
                        LLF: null,
                        HRF: null,
                        MRF: null,
                        LRF: null,
                        HLB: null,
                        MLB: null,
                        LLB: null,
                        HRB: null,
                        MRB: null,
                        LRB: null
                    }
                };
            },

            newCreateListEntry() {
                return {
                    wcid: null,
                    palette: null,
                    shade: null,
                    destination: 0,
                    stack_size: 0,
                    try_to_bond: 0
                };
            },

            newEmote(categoryId) {
                return {
                    emotes: [],
                    category: categoryId,
                    classID: null,
                    maxhealth: null,
                    minhealth: null,
                    NewEmoteType: null,
                    probability: null,
                    quest: null,
                    style: null,
                    substyle: null,
                    vendorType: null
                };
            },
            newEmoteCategory(categoryId) {
                return {
                    key: categoryId,
                    value: [this.newEmote(categoryId)]
                };
            },
            newEmoteAction(actionType) {
                return {
                    type: actionType,
                    delay: 0,
                    extent: 1,
                    amount: null,
                    motion: null,
                    msg: null,
                    amount64: null,
                    heroxp64: null,
                    cprof: null,
                    min64: null,
                    max64: null,
                    percent: null,
                    display: null,
                    max: null,
                    min: null,
                    fmax: null,
                    fmin: null,
                    stat: null,
                    pscript: null,
                    sound: null,
                    mPosition: null,
                    frame: null,
                    spellid: null,
                    teststring: null,
                    wealth_rating: null,
                    treasure_class: null,
                    treasure_type: null,
                };
            },

            newGeneratorListEntry() {
                return {
                    delay: null,
                    frame: this.newFrame(),
                    initCreate: 0,
                    maxNum: 0,
                    objcell_id: 0,
                    probability: null,
                    ptid: null,
                    shade: null,
                    slot: null,
                    stackSize: null,
                    type: null,
                    whenCreate: null,
                    whereCreate: null,
                    deleted: false
                };
            },

            newSkill(skillId) {
                return {
                    key: skillId,
                    value: {
                        sac: null,
                        init_level: null
                    }
                };
            }

        },
        created() {
            if (this.weenieId !== undefined && this.weenieId !== 0) {
                this.fetch(this.weenieId);
            }
        }
    });
}

const WeenieStore = {
    install(Vue, options) {
        Vue.mixin({
            beforeCreate() {
                const opts = this.$options;
                if (opts.weenieStore) {
                    this.$weenie = opts.weenieStore;
                } else if (opts.parent && opts.parent.$weenie) {
                    this.$weenie = opts.parent.$weenie;
                }
            }
        });
    }
};
