/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace DerethForever.Web.Models.Discord
{
    public class WorldReleaseEvent : IDiscordMessage
    {
        public string User { get; set; }

        public DateTimeOffset ReleaseTime { get; set; }
        
        public string Name { get; set; }

        public string Size { get; set; }

        public Message GetDiscordMessage()
        {
            string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            string siteTitle = ConfigurationManager.AppSettings["SiteTitle"];

            Embed embed = Embed.GetDefault(User, ReleaseTime);

            embed.Title = $"A new World Release was created by {User}";
            embed.Description = $"{Name} ({Size}) is now available for download.";

            embed.Fields = new List<Field>();

            string downloadUrl = $"{siteUrl}/WorldRelease/Get?fileName={Name}";

            string links = "";
            links += $"[Download Release]({downloadUrl})";
            links += $" | [{siteTitle}]({siteUrl})";

            embed.Fields.Add(new Field { Name = "Links", Value = links });

            return new Message(embed);
        }
    }
}
