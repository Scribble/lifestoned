﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lifestoned.DataModel.Shared
{
    public enum ContentType
    {
		Unknown,
		Weenie,
		Landblock,
		Recipe,
		Spell,
		Quest,
		Event
    }
}
