﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lifestoned.DataModel.Shared
{
    public enum VendorEmoteType
    {
        Invalid = 0,
        Open = 1,
        Close,
        Sell,
        Buy,
        HeartBeat
    }
}
